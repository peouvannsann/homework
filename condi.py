try:
    num = int(input("Enter a number: "))
    sumnum = []
    #get opposite number of prime 

    a = 'and'
    if(num):
        #if user input num bigger than 10 
        if num > 10 :
            for i in range(2, 11):#start from 2 end at position 9
                if(num % i == 0):# isn't prime      
                    sumnum.append(i)#limite add. End position at 9
        #if num less than 10
        elif num <= 10:
            for i in range(2, num):#start from 2 to num user input
                #if user input num = 10: [10%2=]
                if(num % i == 0):#if isn't prime
                    sumnum.append(i)#divide and add to list
        #print(sumnum) #if you want to check not prime number in list[]

        if sumnum:# if opposite number of prime in list
            if num <= 5:
                if num % 2 == 0:
                    print(f"=> {num} is an EVEN number" )
            elif num <= 180:
                if(num % 2 == 0):
                    print(f"=> {num} is an EVEN and is also divisible by ", *sumnum)                 
            else:            
                print(f"=> {num} is an ODD and is also divisible by ", *sumnum)        
        else:
            if(num % 2 == 0):
                print(f"=> {num} is an EVEN and is also prime number")
            else:
                print(f"=> {num} is an ODD and is also a prime number")
    else:
        print("{} is not prime number".format(num))
except:
    print("Invalid, please input number")

import copy
import csv
from timeit import default_timer as timer


def bubble_sort(numbers_to_sort):

  please_loop = True
  sorted_list = copy.deepcopy(numbers_to_sort)

  while please_loop:

    please_loop = False

    for i in range(1, len(sorted_list)):

      if sorted_list[i] < sorted_list[i - 1]:

        please_loop = True

        temp = sorted_list[i] 
        sorted_list[i] = sorted_list[i-1] 
        sorted_list[i-1] = temp 
	
  return sorted_list

def bubble_file():
    title_list = ['N', 'bubble sort']
    # sort the list using bubble sort
    with open('bubble.csv','w',newline='') as f1:

        writer = csv.writer(f1)
        writer.writerow(title_list)
        for i in range(1,1001):
            start = timer()
            print(bubble_sort(numbers))
            end = timer()
            done = end - start
            print('DURATION BUBBLE SORT :',done)
            row = [i, done]
            writer.writerow(row)

def insertion_sort(numbers_to_sort):

  sorted_list = copy.deepcopy(numbers_to_sort)

  for i in range(1, len(sorted_list)):
    for j in range(i, 0, -1):

      if sorted_list[j - 1] <= sorted_list[j]:
        break
    
      sorted_list[j], sorted_list[j - 1] = sorted_list[j - 1], sorted_list[j]

  return sorted_list

def insertion_file():
    title_list = ['N','insertion sort']
    with open('insertion.csv','w',newline='') as f1:

        writer = csv.writer(f1)
        writer.writerow(title_list)
        for i in range(1,1001):
            start = timer()
            print(insertion_sort(numbers))
            end = timer()
            done = end - start
            print('DURATION INSERTION SORT :',done)
            row = [i, done]
            writer.writerow(row)

def selection_sort(numbers_to_sort):

  sorted_list = copy.deepcopy(numbers_to_sort)

  num = range(len(sorted_list))

  for i in num:
    
    mininum = i 

    for j in range(i+1, len(sorted_list)):

      if sorted_list[j] < sorted_list[mininum]:
        mininum = j
    
    temp = sorted_list[i]
    sorted_list[i] = sorted_list[mininum]
    sorted_list[mininum] = temp
  
  return sorted_list

def selection_file():
    title_list = ['N','selection sort']
    with open('selection.csv','w',newline='') as f1:

        writer = csv.writer(f1)
        writer.writerow(title_list)
        for i in range(1,1001):
            start = timer()
            print(selection_sort(numbers))
            end = timer()
            done = end - start
            print('DURATION SELECTION SORT :',done)
            row = [i, done]
            writer.writerow(row)

def dummy(my_list):

    for item in my_list:
        if item >=0:
            break
    return item

def dummy_file():
    title_list = ['N','dummy']
    with open('dummy.csv','w',newline='') as f1:

        writer = csv.writer(f1)
        writer.writerow(title_list)
        for i in range(1,1001):
            start = timer()
            print(dummy(numbers))
            end = timer()
            done = end - start
            print(done)
            row = [i, done]
            writer.writerow(row)

def no_thing(my_list):
    for item in my_list:
        continue

def no_thing_file():
    title_list = ['N','no thing']
    with open('nothing.csv','w',newline='') as f1:

        writer = csv.writer(f1)
        writer.writerow(title_list)
        for i in range(1,1001):
            start = timer()
            print(no_thing(numbers))
            end = timer()
            done = end - start
            print(done)
            row = [i, done]
            writer.writerow(row)

numbers = [12, -2, 4, 8, 29, 45, 78, 36, -17, 2, 12, 12, 3, 3, -52]

print(bubble_file())
print(insertion_file())
print(selection_file())
print(dummy_file())
print(no_thing_file())

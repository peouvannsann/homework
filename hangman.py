from os import system, name
from time import sleep
import random

words = ['watch', 'glasses', 'laptop', 'mouse', 'computer', 'mask',
         'corona', 'virus', 'python', 'algorithm', 'data', 'structure', 'java', 'javascript', 'environment', 'bluetooth']
attempt = 10
user_guessls = []
userguess = []


while True:
    
    print('\nWelcome to HANGMAN GAME! ')
    print('''Rule: 
1. Available only 1 letter 
2. No number
3. No syntax \n''')

    secret_word = random.choice(words)
    secret_wordls = list(secret_word)

    for i in secret_wordls:
        user_guessls.append('_')
    print("Your Secret word is: " + ''.join(user_guessls).upper())

    # Game Start
    while True:

        letter_input = input('\nENTER LETTER : ')

        # check value user input
        if len(letter_input) > 1 or letter_input.isalpha() == False:

            print('INVALID {}'.format(letter_input))
            continue

        if letter_input in userguess:
            print('ALREADY GUESSED: ', *userguess)

        else:

            userguess.append(letter_input)
            print("LETTER USED: ", *userguess)

            for w in range(len(secret_wordls)):

                if letter_input == secret_wordls[w]:

                    indexletter = w
                    user_guessls[indexletter] = letter_input.upper()

            print("YOUR SECRET WORD IS: " + ''.join(user_guessls).upper())

            if letter_input not in secret_wordls:
                attempt -= 1

            if attempt > 0:
                print('CHANCE LEFT', attempt)

        joinedList = ''.join(user_guessls)

        if joinedList.upper() == secret_word.upper():
            print('\nCONGRATULATION!, YOU WON THE GAME.')
            break

        elif attempt == 0:
            print('\nGAME OVER\nTHE WORD IS :' + secret_word.upper())
            break

    continue_game = input('Do you want to try again? Y or any key to exit')

    if continue_game.upper() == 'Y':
        user_guessls = []
        userguess = []        
        attempt = 10
        sleep(2)
        system('cls')
    else:
        print('THANK FOR PLAYING')
        break

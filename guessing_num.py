import random

while True:
    chance = 10  
    print('\nWelcome to GUESS NUMBER GAME')
    print('You have 10 chance to guess the number.')
    rand_num = random.randint(1, 1000)  
    
    while chance >= 1:
        try:
            num_user = input('\nGUESS NUMBER (1- 1000) {}\n==>'.format(chance))
            num_user = int(num_user)
            
            #Check Invalid input
            if num_user < 0 or num_user >=1000:
                print('Invalid number')
                continue

            if num_user == rand_num:
                print('you won! {}'.format(rand_num))
                break

            elif num_user < rand_num:
                print('Your guess too low, bigger than {}'.format(num_user))

            else:
                print('Your guess too hight, less than {}'.format(num_user))

            chance -= 1

        except ValueError:
            print('invalid input', {num_user})

    if  chance <= 1:
        print('GAME OVER! the number is :{}'.format(rand_num))
    
    try_again = input('Do you to try again? (y/n)')
    
    if try_again != 'y':
        break

print('THANK YOU FOR PLAYING')

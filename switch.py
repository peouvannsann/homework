import math
menu_login = True
case_cd = True
w = ''
hex_to_decimal = {
    "0": 0, "1": 1, "2": 2, "3": 3, "4": 4, "5": 5,
            "6": 6, "7": 7, "8": 8, "9": 9, "A": 10,
            "B": 11, "C": 12, "D": 13, "E": 14, "F": 15
}
while(menu_login):
    welcome = '''
Menu
Please choose a conversion mode (from 1 to 6)
1: binary -> decimal
2: binary -> hexadecimal
3: decimal -> binary
4: decimal -> hexadecimal
5: hexadecimal -> decimal
6: hexadecimal -> binary
      '''
    print(welcome)
    try:
        num_menu = int(input("number"))
        if(num_menu == 1):
            while case_cd:
                string = input('Please number you want to convert: ')
                p = set(string)
                s = {'0', '1'}
                if s == p or p == {'0'} or p == {'1'}:
                    int(string)
                    decimal = 0
                    for i in string:
                        decimal = decimal*2 + int(i)
                    print(f"{string}[2]==" + str(decimal)+"[10]")
                    case_cd = False
                else:
                    print('The number entered is not of the fomat chosen')
                    case_cd = True
                menu_login = False
        elif(num_menu == 2):
            while case_cd:
                string = input('Please number you want to convert: ')
                p = set(string)
                s = {'0', '1'}
                hex = {10: "A", 11: "B", 12: "C", 13: "D", 14: "E", 15: "F"}
                a = ""
                if s == p or p == {'0'} or p == {'1'}:
                    int(string)
                    decimal = 0
                    for i in string:
                        decimal = decimal*2 + int(i)
                    while(True):
                        rem = math.floor(decimal % 16)
                        if rem > 9:
                            a += hex[rem]
                        else:
                            a += str(rem)
                        decimal = math.floor(decimal/16)
                        if decimal == 0:
                            break
                    print(f"{string}[2] ==", a[::-1]+"[16]")
                    case_cd = False
                else:
                    print('The number entered is not of the fomat ')
                    case_cd = True
                menu_login = False
        elif(num_menu == 3):
            while case_cd:
                try:
                    num = int(input("Please number you want to convert: "))
                    rem = ""
                    sum = num
                    if num >= 0:
                        while True:
                            rem += str(num % 2)
                            num = math.floor(num/2)
                            if num == 0:
                                break
                    print(f"{sum}[10] == ", rem[::-1]+"[2]")
                    case_cd = False

                    menu_login = False
                except:
                    print("The number entered is not of the fomat ")
        elif num_menu == 4:
            while case_cd:
                hex = {10: "A", 11: "B", 12: "C", 13: "D", 14: "E", 15: "F"}
                a = ""
                try:
                    num = int(input('Please number you want to convert: '))
                    sum = num
                    if num >= 0:
                        while True:
                            rem = math.floor(num % 16)
                            if rem > 9:
                                a += hex[rem]
                            else:
                                a += str(rem)
                            num = math.floor(num/16)
                            if num == 0:
                                break
                        print(f"{sum}[10] ==", a[::-1]+"[16]")
                        case_cd = False

                    menu_login = False
                except ValueError:
                    print("The number entered is not of the fomat ")
        elif num_menu == 5:
            i = 0
            while case_cd:
                try:
                    num = input('Enter hexadecimal: ')
                    if(hex_to_decimal):
                        for h in num.upper():
                            i = i * 16 + hex_to_decimal[h]
                        print(f"{num}[16] == ", str(i)+"[10]")
                    case_cd = False
                except KeyError:
                    print(f'{num} invalid')
                menu_login = False
        elif num_menu == 6:
            rem = ''
            i = 0
            n = 0
            while case_cd:
                try:
                    num = input('Enter hexadecimal: ')
                    for h in num.upper():
                        i = i * 16 + hex_to_decimal[h]
                        int(i)
                    while(n < i):
                        rem += str(i % 2)
                        int(i)
                        i = math.floor(i/2)
                    if not rem:
                        print('0')
                    else:
                        print(f"{num}[16] ==", rem[::-1]+"[2]")
                    case_cd = False
                except KeyError:
                    print('invalid')
                menu_login = False
        else:
            print('The value enterd is not part of the menu')
    except ValueError:
        print("can not be string")
